<?php
// On définit un login et un mot de passe de base pour tester notre exemple. Cependant, vous pouvez très bien interroger votre base de données afin de savoir si le visiteur qui se connecte est bien membre de votre site
$Nom=$_POST["login"];
$Mdp=$_POST["pwd"];

$bdd = new PDO('mysql:host=localhost;dbname=allogastro', 'root', '');

//verifier le login
$idNomreq=$bdd->query('SELECT nomLogin FROM login WHERE nomLogin="'.$Nom.'"');
$idNomrep =$idNomreq->fetch();
$idNom= $idNomrep['0'];
$idMdpreq=$bdd->query('SELECT mdpLogin FROM login WHERE nomLogin="'.$Nom.'" AND mdpLogin="'.$Mdp.'"');
$idMdprep =$idMdpreq->fetch();
$idMdp= $idMdprep['0'];

//Modifier table
$idIdreq=$bdd->query('SELECT idLogin FROM login WHERE nomLogin="'.$Nom.'"');
$idIdrep =$idIdreq->fetch();
$idId= $idIdrep['0'];
session_start();
$Mdp2 = password_hash($Mdp, PASSWORD_DEFAULT);

if(empty($Nom)){
	unset($Nom);
	
	$msglog = '<p style="text-align:center;color:red"> Veuillez indiquer votre login svp ! </p>';
	$_SESSION['msglog']=$msglog;

	}
if(empty($Mdp)){
	unset($Mdp);
	$msglog = '<p style="text-align:center;color:red"> Veuillez indiquer votre mot de passe svp ! </p>';
	$_SESSION['msglog']=$msglog;

}
// on teste si nos variables sont définies

if (isset($Nom, $Mdp)) {
	//login BON
	// on vérifie les informations du formulaire, à savoir si le pseudo saisi est bien un pseudo autorisé, de même pour le mot de passe
	// if (($idNom == $Nom) && ($idMdp == $Mdp)) {
     if(password_verify($Mdp, $Mdp2)) {
		
		
		$_SESSION['login'] = $Nom;
		$_SESSION['pwd'] = $Mdp;
		// on redirige notre visiteur vers une page de notre section membre
		header ('location: ../page_membre.php');

		
	}
	else {

		$_SESSION['login'] = $Nom;
		$_SESSION['pwd'] = $Mdp;
		 // Le pseudo est-il correct ?
		if($Nom !== $idNom){
			$msglog = '<p style="text-align:center;color:red"> Votre login est faux ! </p>';
		}
		 // Le mot de passe est-il correct ?
		 elseif($Mdp !== $idMdp){
			$msglog = '<p style="text-align:center;color:red"> Votre mot de passe est faux ! </p>';
			}
		$_SESSION['msglog']=$msglog;
		header ('location: ../connexion.php');
		
    }
}
else {
		// Le visiteur n'a pas été reconnu comme étant membre de notre site.
		$_SESSION['login'] = $Nom;
		$_SESSION['pwd'] = $Mdp;
		$msglog = '<p style="text-align:center;color:red"> Erreur veuillez remplir tous les champs. </p>';
		$_SESSION['msglog']=$msglog;
		
		header ('location: ../connexion.php');
		
	}
?>