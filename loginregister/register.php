<?php
// On définit un login et un mot de passe de base pour tester notre exemple. Cependant, vous pouvez très bien interroger votre base de données afin de savoir si le visiteur qui se connecte est bien membre de votre site
$Nom=$_POST["login"];
$Mdp=$_POST["pwd"];
$confMdp=$_POST["confpwd"];

$bdd = new PDO('mysql:host=localhost;dbname=allogastro', 'root', '');

//verifier le login
$idNomreq=$bdd->query('SELECT nomLogin FROM login WHERE nomLogin="'.$Nom.'"');
$idNomrep =$idNomreq->fetch();

session_start();
if($Nom==''){
	unset($Nom);
	$msgreg = '<p style="text-align:center;color:red"> Veuillez indiquer votre login svp ! </p>';
	$_SESSION['msgreg']=$msgreg;
	}
if($Mdp==''){
	unset($Mdp);
	$msgreg = '<p style="text-align:center;color:red"> Veuillez indiquer votre mot de passe svp ! </p>';
	$_SESSION['msgreg']=$msgreg;
	}
if($confMdp==''){unset($confMdp);
	$msgreg = '<p style="text-align:center;color:red"> Veuillez confirmer votre mot de passe svp ! </p>';
	$_SESSION['msgreg']=$msgreg;
	}
// on teste si nos variables sont définies
if (isset($Nom, $Mdp,$confMdp)) {
		if($confMdp != $Mdp){
			$msgreg = '<p style="text-align:center;color:red"> Erreur Mots de passe différent ! </p>';
			$_SESSION['msgreg']=$msgreg;
			header ('location: ../connexion.php');
		}
		else{
			// on vérifie les informations du formulaire, à savoir si le pseudo saisi est bien un pseudo autorisé, de même pour le mot de passe
			if($idNomrep == false){
				//Modifier table
				$Mdp2=password_hash($Mdp, PASSWORD_DEFAULT);
				$register=$bdd->query('INSERT INTO login (mdpLogin,nomLogin) VALUES ("'.$Mdp2.'","'.$Nom.'")');

				$msgreg = '<p style="text-align:center;color:green"> Inscription Réussie :) </p>';
				
				$_SESSION['msgreg']=$msgreg;
				// on redirige notre visiteur vers une page de notre section membre
				header ('location: ../connexion.php');
				//var_dump($Mdp, $Mdp2);
			}
			else {
				// Le visiteur n'a pas été reconnu comme étant membre de notre site.
				$msgreg = '<p style="text-align:center;color:red"> Erreur pseudo déjà utilisé ! </p>';
				$_SESSION['msgreg']=$msgreg;
				// puis on le redirige vers la page d'accueil
				header ('location: ../connexion.php');
			}
		}
}
else {
		// Le visiteur n'a pas été reconnu comme étant membre de notre site.
		$msgreg = '<p style="text-align:center;color:red"> Veuillez remplir tous les champs ! </p>';
		$_SESSION['msgreg']=$msgreg;
		// puis on le redirige vers la page d'accueil
		header ('location: ../connexion.php');
	}

?>