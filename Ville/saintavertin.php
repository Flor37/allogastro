<!DOCTYPE HTML> <!--déclare type html-->
<?php
error_reporting(0);
$bdd = new PDO('mysql:host=localhost;dbname=allogastro', 'root','' );
session_start();
$Nom= $_SESSION['login'];
$Mdp= $_SESSION['pwd'];

?>
<html>
<head>   <!--Contient les métadonnée + liens-->
<html lang="fr">
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html">
<meta name="author" content="Gregory/florian">
<meta name="description" content="desc">
<meta name="keyword" content="Nourriture,local, locavore,tours, tourangeau">
<link rel="stylesheet" type="text/css" href="../css/css.css" />
<link rel="stylesheet" type="text/css" href="../css/csscatego.css" />
<link rel="stylesheet" type="text/css" href="../css/button.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="icon" sizes="128x128" type="image/ico" href="../icon.ico"/>
<title>AlloGastro</title>
</head>
<body style="margin:0px; background-color:rgb(232,232,232)">
<div class="entete">
		<div id="header2">
			<div class="title">

				<a><img src="../logo.png" width="42" height="42"> </a>
				<a> Allogastro.com</a>
				<a><img src="../logo.png" width="42" height="42">  </a>
				
			</div>
		</div>
			<div id="header"> 
				<div class="topnav">
					<a href="../index.php"> ACCUEIL</a>

					<a href="../categorie.php">  CATÉGORIE </a>
					<?php
						
						
						if (isset($Nom) && isset($Mdp)) {
							echo '<a href="../page_membre.php">  ESPACE MEMBRE </a>';
						}
						else{
							echo '<a href="../connexion.php">  ESPACE MEMBRE </a>';
						}
					?>
					<div class="search-container">
						<form method="post" action="../recherche.php">
							<input type="text" name="recherche" placeholder="Recherche">
							<button type="submit" value="recherche"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
			
			</div> 
</div>

<ul id="menu-accordeon">
	<li><a>Catégorie</a>
      <ul>
			<li><a href="../categorie/viande.php">Viande</a></li>
			<li><a href="../categorie/confiserie.php"> Confiserie </a></li>
			<li><a href="../categorie/feculent.php">Féculent </a></li>
			<li><a href="../categorie/gateau.php">Gateau </a></li>
			<li><a href="../categorie/boisson.php">Boisson</a></li>	
      </ul>
	</li>
	<li><a href="#">Villes</a>
      <ul>
			<li><a href="tours.php">Tours</a></li>
			<li><a href="azaylerideau.php">Azay-le-Rideau</a></li>
			<li><a href="jouelestours.php">Joué-les-Tours </a></li>
			<li><a href="chambraylestours.php">Chambray-les-Tours </a></li>
			<li><a href="amboise.php">Amboise</a></li>	
			<li><a href="azaysurcher.php">Azay-sur-Cher </a></li>
			<li><a href="saintavertin.php">Saint-Avertin</a></li>	
			<li><a href="saintpierredescorps.php">Saint-Pierre-des-Corps</a></li>	
      </ul>
	</li>

</ul>
<br></br>
<div style="position: fixed;right:20px ; margin-top:110px;font-size:18px"> Saint-Avertin </div>
<div class="confiserie">

  <?php
	$bdd = new PDO('mysql:host=localhost;dbname=allogastro', 'root','' );
	
	$idCategoreq = $bdd->query('SELECT DISTINCT nourriture.nomNourriture FROM nourritureex,ville,nourriture WHERE nourriture.idNourriture = nourritureex.idNourritureex AND nourritureex.idVille = ville.idVille AND ville.nomVille LIKE "Saint-Avertin"')-> fetchAll();
	$idCategocount= count($idCategoreq);
	$_SESSION["ville"]= "Saint-Avertin";
	for ($x = 0; $x < $idCategocount; $x++) {
						$aliment = $idCategoreq[$x];
						echo '<form  method="post" action="../rechercheville.php"><button style=" cursor:pointer" type="submit" name="recherche" class="button" value="'.$aliment[0].'" ><i  style="text-decoration:none"> '.$aliment[0].'</i></button></form>';
						echo '<br></br>';
					
	}
?>

  
</div>


</body>
</html>
